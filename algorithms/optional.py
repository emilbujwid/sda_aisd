class OptionalAlgorithms:
    @staticmethod
    def square_digit(val):
        return val


if __name__ == "__main__":
    sda_opt = OptionalAlgorithms()

    assert sda_opt.square_digit(123) == 146, "Square_digit is not working well yet!"
    assert sda_opt.square_digit(9119) == 811181, "Square_digit is not working well yet!"
    print("looks like square_digit is working!")